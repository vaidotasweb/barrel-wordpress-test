<?php
/**
 * Post block
 * @author BarrelNY
 */

$front_page__id = 2;
$front_page = get_post($front_page__id);
$front_page__headline = $front_page->index__headline;
$front_page__content = $front_page->post_content;
$front_page__product_image__id = $front_page->index__product;
$front_page__product_image__src = wp_get_attachment_image_url($front_page__product_image__id, 'full');
$front_page__product_image = ''.$front_page__product_image__src.'';
$front_page__background_image = featured_image_or_fallback($front_page);

?>

<div class="hero" data-module="hero">
  
  <div class="container">
    <div class="hero-feature" style="background-image:url('<?php echo $front_page__background_image; ?>')">
      <div class="hero-feature__wrapper">
        <h1><?php echo $front_page__headline; ?></h1>
        <p><?php echo $front_page__content; ?></p>
      </div>
      <div class="hero-feature__wrapper">
        <div class="post__image">
          <span style="background-image:url('<?php echo $front_page__product_image; ?>')"></span>
        </div>
      </div>
    </div>
  </div>

</div>