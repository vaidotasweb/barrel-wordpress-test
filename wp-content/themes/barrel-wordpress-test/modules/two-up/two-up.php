<?php
/**
 * Post block
 * @author BarrelNY
 */

$feature_post__id = 1;
$feature_post = get_post($feature_post__id);
$feature_post__content = $feature_post->post_content;
$feature_post__image = featured_image_or_fallback($feature_post);
$feature_post__icon = get_format_icon($feature_post);
$feature_post__cta = get_cta($feature_post);

?>

<div class="two-up" data-module="two-up">

  <div class="two-up-post">
    <div class="two-up-post__wrapper-image">
      <a href="<?php the_permalink(); ?>" class="post--link">
        <div class="post__image" style="background-image:url('<?php echo $feature_post__image; ?>')">
        </div>
      </a>
    </div>
    <div class="two-up-post__wrapper-meta">
      <div class="post__meta">
        <div class="post__meta--icon">
          <?php echo $feature_post__icon; ?>
        </div>
        <a href="<?php echo get_month_link(get_the_date('Y'), get_the_date('m')); ?>" class="post__meta--date">
          <button><?php echo get_the_date('F d'); ?></button>
        </a>
        <a href="<?php the_permalink(); ?>" class="post__meta--title post--link">
          <h4><?php the_title(); ?></h4>
        </a>
        <a href="<?php the_permalink(); ?>" class="post--link">
          <p><?php echo $feature_post__content; ?></p>
        </a>
        <a href="<?php the_permalink(); ?>" class="post__meta--cta post--link">
          <?php echo $feature_post__cta; ?>
        </a>
      </div>
    </div>
  </div>

</div>